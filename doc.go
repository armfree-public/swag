/*
Package swag converts Go annotations to Swagger Documentation 2.0.
See https://gitlab.com/armfree-public/swag for more information about swag.
*/
package swag // import "gitlab.com/armfree-public/swag"
